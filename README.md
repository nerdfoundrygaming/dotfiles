# dotfiles

This is the dotfiles for nfgCodex.

The End.

## Prequisites

Assumes `zsh` environment

Need the following packages installed:

* stow
* figlet

## Setup

You can either directly `stow` individual apps, or just run `setup.sh` to do the whole gamut.

> If a file already exists on the filesystem, `stow` will bomb out any future file operations.
> To resolve this, delete the original file (such as one created by default by an installed app).
> An example of this is `~/.zshrc`

# TODO

* Finish all other packages
    * Split packages into per-app (currently is just "home")
* wsl setup script?
    * Preinstall all desired packages
* debian-variant (should be the same as wsl?)
* alpine-variant
* establish local scripts for each environment (ie, desktop vs laptop vs servers)