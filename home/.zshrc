########################################################################
# ENV Vars

export ZSH="/home/nfg/.oh-my-zsh"
export EDITOR='vim'
export DISPLAY="`grep nameserver /etc/resolv.conf | sed 's/nameserver //'`:0"
export DOCKER_NODE_IMAGE=node:lts-alpine

ZSH_THEME="agnoster"
DISABLE_UPDATE_PROMPT="true"
COMPLETION_WAITING_DOTS="true"
plugins=(git docker)

source $ZSH/oh-my-zsh.sh

########################################################################
# Aliases

alias ll="ls -la --color=auto"
alias dpss="docker ps -a --format=\"table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.RunningFor}}\t{{.Status}}\""
alias dps="docker ps -a --format=\"table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.RunningFor}}\t{{.Status}}\t{{.Ports}}\""
alias cat="batcat"
alias ash="docker run -it --rm --name alpine $DOCKER_NODE_IMAGE ash"
alias tmuxx="tmux attach || tmux"

function npm() {
  nodeAlpine npm $@
}

function npx() {
  nodeAlpine npx $@
}

function nodeAlpine() {
  docker run --rm -it -u $(id -u) -w /npx -v $(pwd):/npx $DOCKER_NODE_IMAGE $@
}

########################################################################
# MOTD Display

motd_stamp="/tmp/.motd_shown_$(whoami)"

function print_header() {
    figlet nfgCodex
    echo "\t\t$(date)"    
}

# ZSH MOTD - once every 3 hours
if ! find $motd_stamp -mmin -59 2> /dev/null | grep -q -m 1 '.'; then
    print_header
    touch $motd_stamp
fi

if [ -f ./.zshrc_local ]; then
    source ./.zshrc_local
fi
